#!/bin/bash

cd Framework

WORKSPACE_NAME="Framework"
FRAMEWORK_NAME="Reachability"
SCHEME_NAME="iOSReachability"
SIMULATOR_FRAMEWORK="./build/derivedData/Build/Products/Release-iphonesimulator/$FRAMEWORK_NAME.framework"
IPHONE_FRAMEWORK="./build/derivedData/Build/Products/Release-iphoneos/$FRAMEWORK_NAME.framework"
OUTPUT_FRAMEWORK="../SDFramework/$FRAMEWORK_NAME.xcframework"

rm -rf $SIMULATOR_FRAMEWORK  > /dev/null 
rm -rf $IPHONE_FRAMEWORK > /dev/null
rm -rf $OUTPUT_FRAMEWORK > /dev/null
rm -rf ./build/derivedData > /dev/null


#编译模拟器
xcodebuild -project "$WORKSPACE_NAME.xcodeproj"\
  -scheme  "$SCHEME_NAME" -configuration "Release"\
  -sdk "iphonesimulator"\
  -arch x86_64 \
  -derivedDataPath "build/derivedData"\
  -archivePath "./build/simulator.xcarchive"\
  SKIP_INSTALL=NO BUILD_LIBRARIES_FOR_DISTRIBUTION=YES MACH_O_TYPE=staticlib

#编译真机
xcodebuild -project "$WORKSPACE_NAME.xcodeproj"\
  -scheme "$SCHEME_NAME"\
  -arch arm64 \
  -configuration "Release"\
  -sdk "iphoneos"\
  -derivedDataPath "build/derivedData"\
  -archivePath "./build/ios.xcarchive"\
  SKIP_INSTALL=NO BUILD_LIBRARIES_FOR_DISTRIBUTION=YES MACH_O_TYPE=staticlib

#编译MacOS
# xcodebuild -workspace "$WORKSPACE_NAME.xcworkspace"\
#  -scheme "$FRAMEWORK_NAME"\
#  -configuration "Release"\
#  -destination 'platform=macOS,arch=x86_64,variant=Mac Catalyst'\
#  -sdk "$MAC_OS"\
#  -derivedDataPath "build/derivedData"\
#  -archivePath "./build/catalyst.xcarchive"\
#  BUILD_LIBRARIES_FOR_DISTRIBUTION=YES SUPPORTS_MACCATALYST=YES SKIP_INSTALL=NO clean

#合并库 
xcodebuild -create-xcframework \
-framework ${SIMULATOR_FRAMEWORK} \
-framework ${IPHONE_FRAMEWORK} \
-output ${OUTPUT_FRAMEWORK}

#zip -r XC_FRAMEWORK.zip "./$WORKSPACE_NAME.xcframework"